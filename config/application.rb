require File.expand_path('../boot', __FILE__)

require 'rails/all'

# Require the gems listed in Gemfile, including any gems
# you've limited to :test, :development, or :production.
Bundler.require(:default, Rails.env)

module Ingennia
  class Application < Rails::Application
    # Settings in config/environments/* take precedence over those specified here.
    # Application configuration should go into files in config/initializers
    # -- all .rb files in that directory are automatically loaded.

    # Set Time.zone default to the specified zone and make Active Record auto-convert to this zone.
    # Run "rake -D time" for a list of tasks for finding time zone names. Default is UTC.
    # config.time_zone = 'Central Time (US & Canada)'

    # The default locale is :en and all translations from config/locales/*.rb,yml are auto loaded.
    # config.i18n.load_path += Dir[Rails.root.join('my', 'locales', '*.{rb,yml}').to_s]
     config.i18n.default_locale = :es

    config.action_mailer.smtp_settings = {
        :address              => "smtp.zoho.com",
        :port                 => 465,
        :domain               => "riggingsteel.cl",
        :user_name            => "mailer@riggingsteel.cl",
        :password             => "r1gg1ngst33l",
        :authentication       => :login,
        :ssl                  => true,
        :tls                  => true,
        :enable_starttls_auto => true
    }

    config.action_mailer.default_url_options = {
        :host => "riggingsteel.cl"
    }

  end
end
