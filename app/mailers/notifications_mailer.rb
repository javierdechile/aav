class NotificationsMailer < ActionMailer::Base

  default :from => "mailer@riggingsteel.cl"
  default :to => "contacto@riggingsteel.cl"

  def new_message(message)
    @message = message
    mail(:subject => "[Formulario RiggingSteel] #{message.subject}")
  end

end
